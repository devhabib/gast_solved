let $ = jQuery;

$(function () {
    let wrapper = $('.slide-wrapper > .slide-item');
    let btn = $('#has-btn');
    wrapper.hide();
    wrapper.slice(0, 6).show();
    btn.on('click', function (e) {
        e.preventDefault()
        $('.slide-wrapper > .slide-item:hidden').slice(0, 3).slideDown();
        if ($('.slide-wrapper > .slide-item:hidden').length == 0) {
            btn.fadeOut('slow');
        }
    })
})